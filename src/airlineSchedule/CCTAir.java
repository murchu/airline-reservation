package airlineSchedule;

public class CCTAir {

	public static void main(String args) {
		
		/*
		 *  a. Declare and initialize a number of flights.
			b. Add up to 5 flights of your choice.
			c. Use the second version of the schedule method to set the time schedule for a
			flight.
			d. Use the first version of the schedule method to update the arrival time for a
			flight in the list.
			e. display all flights.
		 */
		
		// pilots
		Pilot pilotOne = new Pilot("John", "Intermediate");
		Pilot pilotTwo = new Pilot("Fred", "Advanced");
		
		// aircrafts
		Airplane airplaneOne = new Airplane("Boeing", "737", 320, pilotOne);
		Airplane airplaneTwo = new Airplane("Airbus", "A330", 265, pilotTwo);
		
		// flights
		Flight flightOne = new Flight("Dublin", "Rio", "10/12/2018", airplaneOne);
		Flight flightTwo = new Flight("Heathrow", "Barcelona", "10/11/2018", airplaneTwo);
		Flight flightThree = new Flight("Paris", "Rome", "20/12/2018", airplaneOne);
		Flight flightFour = new Flight("Rio", "San Francisco", "31/12/2018", airplaneTwo);
		
		System.out.println(flightOne);
		System.out.println(flightTwo);
		System.out.println(flightThree);
		System.out.println(flightFour);
		
		flightOne.schedule("11:00", "20:00");
		flightTwo.schedule("08:00");
		
		System.out.println(flightOne);
		System.out.println(flightTwo);
		System.out.println(flightThree);
		System.out.println(flightFour);
		
	}
	
}
