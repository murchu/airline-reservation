package airlineSchedule;

public class Flight {
	
	/**
	 * 
	 */
	private String origin;
	private String destination;
	private String departureTime;
	private String arrivalTime;
	private String date;
	private Airplane aircraft;
	
	/**
	 * 
	 * Creates a Flight
	 *  
	 * @param origin location flight is leaving from
	 * @param destination location flight is flying to
	 * @param date date of flight
	 * @param aircraft aircraft flown by flight
	 */
	public Flight(String origin, String destination, String date, Airplane aircraft) {
		super();
		this.origin = origin;
		this.destination = destination;
		this.date = date;
		this.aircraft = aircraft;
		this.arrivalTime = "No-Arrival-Time-Yet";
		this.departureTime = "No-Departure-Time-Yet";
	}

	@Override
	public String toString() {
		return "Flight Information:\n" + 
				"Date: " + date + "\n" + 
				"From: " + origin + " to " + destination + "\n" + 
				"Flight time: " + departureTime + " to " + arrivalTime + "\n" + 
				"Plane Information:\n" + 
				"Aircraft : " + aircraft.getMake() + " " + aircraft.getModel() + "\n" + 
				"Capacity: " + aircraft.getCapacity() + " seats\n" + 
				"Pilot: " + aircraft.getPilot();
	}
	
	/**
	 * Sets arrival time of flight
	 * 
	 * @param arrivalTime time flight will arrive
	 */
	void schedule(String arrivalTime) {
		this.arrivalTime = arrivalTime;
	}
	
	/**
	 * Sets arrival and departure time of flight
	 * 
	 * @param arrivalTime time flight will arrive
	 * @param departureTime time flight will depart
	 */
	void schedule(String arrivalTime, String departureTime) {
		this.arrivalTime = arrivalTime;
		this.departureTime = departureTime;
	}
	
}
