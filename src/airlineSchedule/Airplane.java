package airlineSchedule;

public class Airplane {
	
	private String make;
	private String model;
	private int capacity;
	private Pilot pilot;
	
	/**
	 * Creates an Airplane
	 * 
	 * @param make make of airplane
	 * @param model model of airplane
	 * @param capacity seat capacity of airplane
	 * @param pilot pilot that will fly airplane
	 */
	public Airplane(String make, String model, int capacity, Pilot pilot) {
		super();
		this.make = make;
		this.model = model;
		this.capacity = capacity;
		this.pilot = pilot;
	}

	/**
	 * Get make of the airplane
	 * 
	 * @return make of the airplane
	 */
	public String getMake() {
		return make;
	}

	/**
	 * Get model of the airplane
	 * 
	 * @return model of the airplane
	 */
	public String getModel() {
		return model;
	}

	/**
	 * Get seat capacity of airplane
	 * 
	 * @return seat capacity of airplane
	 */
	public int getCapacity() {
		return capacity;
	}

	/**
	 * Get the pilot of the airplane
	 * 
	 * @return pilot of the airplane
	 */
	public Pilot getPilot() {
		return pilot;
	}

	/**
	 * Set the pilot of the airplane
	 * 
	 * @param pilot pilot that will fly airplane
	 */
	public void assignPilot(Pilot pilot) {
		this.pilot = pilot;
	}

	@Override
	public String toString() {
		return "Airplane Information:\n" + 
				"Aircraft : " + make + " " + model + "\n" + 
				"Capacity: " + capacity + " seats\n" + 
				"Pilot: " + getPilot().getName();
	}
	
	
	
}
