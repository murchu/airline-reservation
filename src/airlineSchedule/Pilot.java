package airlineSchedule;

public class Pilot {
	
	private String name;
	private String rating;
	
	public Pilot(String name, String rating) {
		super();
		this.name = name;
		this.rating = rating;
	}

	public String getName() {
		return name;
	}

	public String getRating() {
		return rating;
	}
	
	
}
